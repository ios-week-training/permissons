//
//  CameraButton.swift
//  test1
//
//  Created by Iqbal Rahman on 04/03/24.
//

import SwiftUI

struct CameraButton: View {
    @Binding var isAction: Bool
    
    var body: some View {
        Image(systemName: "camera.fill")
    }
}

struct CameraButtonView: PreviewProvider {
    static var previews: some View {
        CameraButton(isAction: .constant(false))
            .previewLayout(.sizeThatFits)
            .padding()
    }
}
