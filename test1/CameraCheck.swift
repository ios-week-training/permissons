import SwiftUI
import AVFoundation

func checkCameraPermissions(completion: @escaping (Bool) -> Void) {
    switch AVCaptureDevice.authorizationStatus(for: .video) {
    case .authorized:
        completion(true)
        
    case .notDetermined:
        AVCaptureDevice.requestAccess(for: .video) { granted in
            DispatchQueue.main.async {
                completion(granted)
            }
        }
        
    case .denied:
        completion(false)
        
    case .restricted:
        completion(false)
        
    @unknown default:
        completion(false)
    }
}
