//
//  ContentView.swift
//  test1
//
//  Created by Iqbal Rahman on 04/03/24.
//

import SwiftUI
import MapKit
import PhotosUI

struct ContentView: View {
    @State private var showCamera = false
    @State private var selectedImage: UIImage?
    
    var body: some View {
        NavigationStack {
            VStack {
                if let selectedImage{
                    Image(uiImage: selectedImage)
                        .resizable()
                        .scaledToFit()
                }
                Button("Open camera") {
                    self.showCamera.toggle()
                }
                .fullScreenCover(isPresented: self.$showCamera) {
                    CameraView(selectedImage: self.$selectedImage)
                }
                NavigationLink {
                    ShowMap()
                } label : {
                    Text("Open Maps")
                }
                .navigationTitle("Home")
            }
            .padding()
//            .actionSheet(isPresented: $isAction) {
//                ActionSheet(
//                    title: Text("Select Image"),
//                    message: Text("Please select an image from the gallery or use the camera."),
//                    buttons: [
//                        .default(Text("Camera")) {
//                            self.sourceType = 0
//                            self.showImage.toggle()
//                        },
//                        .default(Text("Galery")) {
//                            self.sourceType = 1
//                            self.showImage.toggle()
//                        },
//                        .cancel()
//                    ]
//                )
//            }
//            if showImage {
//                CameraView(sourceType: sourceType)
//            }
        }
    }
}

#Preview {
    ContentView()
}
