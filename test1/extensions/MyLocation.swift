//
//  MyLocation.swift
//  test1
//
//  Created by Iqbal Rahman on 05/03/24.
//

import Foundation
import MapKit

extension CLLocationCoordinate2D {
    static var myLocation: CLLocationCoordinate2D {
        return .init(latitude: 37.334, longitude: -122.34)
    }
}
