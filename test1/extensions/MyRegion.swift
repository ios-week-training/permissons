//
//  MyRegion.swift
//  test1
//
//  Created by Iqbal Rahman on 05/03/24.
//

import Foundation
import MapKit

extension MKCoordinateRegion {
    static var myRegion: MKCoordinateRegion {
        return .init(center: .myLocation, latitudinalMeters: 1000, longitudinalMeters: 1000)
    }
}
