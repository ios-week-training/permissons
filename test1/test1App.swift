//
//  test1App.swift
//  test1
//
//  Created by Iqbal Rahman on 04/03/24.
//

import SwiftUI

@main
struct test1App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
