import CoreLocation
import SwiftUI
import MapKit

final class LocationManager: NSObject, CLLocationManagerDelegate, ObservableObject, MKMapViewDelegate {
    @Published var manager: CLLocationManager = .init()
    @Published var isLocationUnAuthorized: Bool = true
    @Published var userLocation: CLLocationCoordinate2D = .init(latitude: 37, longitude: -122)
    @Published var mapLocation: CLLocationCoordinate2D = .init(latitude: 37, longitude: -122)
    @Published var mapRegion = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 43.457105, longitude: -80.508361), span: MKCoordinateSpan(latitudeDelta: 0.2, longitudeDelta: 0.2))
    
    var binding: Binding<MKCoordinateRegion> {
        Binding {
            self.mapRegion
        } set: { newRegion in
            DispatchQueue.main.async {
                self.mapRegion = newRegion
            }
        }
    }
    
    override init() {
        super.init()
        
        manager.delegate = self
        manager.requestWhenInUseAuthorization()
        manager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
    }
    
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
//        checkLocationAuthorization(status: manager.authorizationStatus)
        checkLocationAuthorization()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let currentLocation = locations.last else{return}
        
        if let location = manager.location {
            DispatchQueue.main.async {
                self.mapRegion = MKCoordinateRegion(center: location.coordinate, span: MKCoordinateSpan(latitudeDelta: 0.2, longitudeDelta: 0.2))
                self.userLocation = .init(latitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude)
                self.mapLocation = .init(latitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude)
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
    }
    
    func checkLocationAuthorization() {
        switch manager.authorizationStatus {
        case .notDetermined:
            print("not determined")
            DispatchQueue.main.async {
                self.manager.requestWhenInUseAuthorization()
            }
        case .restricted, .denied:
            print("Location Authorization Denied or Restricted")
            DispatchQueue.main.async {
                self.manager.requestWhenInUseAuthorization()
            }
        case .authorizedWhenInUse, .authorizedAlways:
            print("auth always")
            isLocationUnAuthorized = false
            DispatchQueue.main.async {
                self.manager.startUpdatingLocation()
            }
        default:
            print("default")
            break
        }
    }
}
