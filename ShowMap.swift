//
//  ShowMap.swift
//  test1
//
//  Created by Iqbal Rahman on 06/03/24.
//

import SwiftUI
import MapKit

struct ShowMap: View {
//    @State var mapRegion: MapCameraPosition = .region(.myRegion)
    @StateObject var locationManager: LocationManager = .init()
    @State var showMap: Bool = false
    
    var body: some View {
        VStack {
            if !locationManager.isLocationUnAuthorized {
                Map(coordinateRegion: locationManager.binding,showsUserLocation: true,userTrackingMode: .constant(.follow))
                        .edgesIgnoringSafeArea(.all)
//                .onAppear(perform: {
//                    mapRegion = .region(MKCoordinateRegion(center: locationManager.userLocation, latitudinalMeters: 1000, longitudinalMeters: 1000))
//                })
            } else {
                ProgressView()
                    .progressViewStyle(.circular)
                Text("Loading ...")
                .alert("Important message", isPresented: .constant(true)) {
                    Button("Grant Permissions", role: .none) {
                        guard let url = URL(string: UIApplication.openSettingsURLString) else { return }
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    }
                    Button("Cancel", role: .cancel) { }
                }
            }
        }
        
    }
}

#Preview {
    ShowMap()
}
